/*
 1V ~ 100°
 5V ~ 1023
 celcius = Analog-OUT*(500/1024)
 temperatur° = Vout * 100
 */
#define LM35 A7
#define KELVIN 273.15

int raw;
float celcius;
float kelvin;

void setup() {
  Serial.begin(9600);
}

void loop() {
  raw = analogRead(LM35);
  celcius = raw * (500/1024.0);
  kelvin = KELVIN + celcius;
  Serial.print("raw: ");
  Serial.print(raw);
  Serial.print(" - ");
  Serial.print(celcius);
  Serial.print("°");
  Serial.print(" - ");
  Serial.print(kelvin);
  Serial.println("K");

  delay(100);

}
