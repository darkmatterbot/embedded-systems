/* Zeilen: PORT-C |  Spalten: PORT-A
  -------------------------
  | S1  | S2  | S3  | S4  | - 0 --> D37
  | S5  | S6  | S7  | S8  | - 1 --> D36
  | S9  | S10 | S11 | S12 | - 2 --> D35
  | S13 | S14 | S15 | S16 | - 3 --> D34
  -------------------------
    |      |     |     |
    7      6     5     4
   D29    D28   D27   D26

Wenn Pin OUTPUT: 1= HIGH,   0=LOW  
Wenn Pin INPUT:  1= ENABLE, 0=Disable  Pull-up Resistor
*/

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);

const uint8_t ROWS[4] = {0,1,2,3};  // Die ersten 4 Bits vom PORT-C
const uint8_t COLS[4] = {7,6,5,4};  // Die letzten 4 Bits vom PORT-A
uint8_t row_i =0;
uint8_t col_i =0;
String pressed_key = "";

void setup() {

  Serial.begin(9600);
  lcd.begin();
  
  DDRA &= B00001111;        // COLS INPUT(0)
  DDRC |= B00001111;        // ROWS OUTPUT(1)
  PORTC = (1 << ROWS[0]);
  
  cli();                    // interrupts deaktivieren

  TCNT1 = 0;                // setze timer1(16bit) auf 0
  TCCR1B |= B00000001;      // kein(1)-prescaler notwendig

  TIMSK1 |= B00000010;      // aktiviert OCIE1A, also den compare match mode für OCR1A
  OCR1A = 16;               // alle 1000ns: 1000(ns)*16.000.000 / ( 1 * 10^(9) ) = 16 


  sei();                    // interrupts aktivieren
}

void loop() {

  lcd.clear();
  lcd.print(pressed_key); 
  Serial.println(pressed_key);
  delay(500);
}


// Timer Interrupt, wird jede ms aufgerufen
ISR(TIMER1_COMPA_vect){
  
  // Welche der 4 Spalten wurde gedrückt?
  if( PINA & (1 << COLS[col_i]) ){
    pressed_key = row_i*4 + col_i + 1;  // matrix[row_i][col_i];
  }

  col_i = (col_i+1) %4;                 // Nächster Spaltenindex
  if(col_i == 0){                       // Näcshte Reihe auf HIGH setzen  
    row_i = (row_i+1) %4;
    PORTC = (1 << ROWS[row_i]);
  }
  
  TCNT1 = 0;                            // setze timer1 wieder auf 0
}
