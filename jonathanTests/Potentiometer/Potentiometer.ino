uint16_t nAD = 0;
float fUe = 0.0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  nAD = analogRead(0);
  fUe = nAD * 0.00488 + 0.00244; // 0.00244

  float r2 = 3600/((5/fUe)-1);
  Serial.print("nAD: ");
  Serial.print(nAD);
  Serial.print(" - fUe: ");
  Serial.print(fUe);
  Serial.print(" - r2: ");
  Serial.println(r2);
  delay(500);
}
