#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <Arduino.h>

const uint8_t btn_Up = 2;
const uint8_t btn_Down = 3;

LiquidCrystal_I2C lcd(0x27, 16, 2);

int cnt =1;
void setup() {
    Serial.print("setup: ");
    Serial.begin(9600);
    lcd.init();
    lcd.backlight();
    DDRE = B00001000; 

    EICRA = (1 << ISC11) | (1 << ISC10);  // external INT1 on rising edge
    EIMSK = (1<<INT4) | (1<<INT5);  // External Interrupt Request 1 Enable
    sei();
}


ISR(INT4_vect) {
  Serial.print("toggle: ");
  Serial.println(cnt++);
}

void loop() {
  
  delay(1000);
}