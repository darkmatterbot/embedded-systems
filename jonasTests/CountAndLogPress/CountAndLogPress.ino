#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#define TESTINT4 (PINE & 0b00010000);
#define TESTINT5 (PINE & 0b00100000);


volatile uint16_t timerTime; 
volatile uint8_t  timerRun = 0;

volatile unsigned long sinceLastInt = 0, debounceTime = 40, timePressed = 0;
volatile uint8_t int4State = 0, int5State = 0;
volatile int cnt = 0;
volatile boolean int4active = false, int5active = false;

LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup() {
  Serial.begin(9600);
  lcd.init();
  lcd.backlight();
  DDRE = B00001000; 
  DDRG = B00100000;
  PORTE = B00000000;
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;

  // Set timer to 1000Hz (16000000 / prescaler (256) / 1000)
  OCR1A = 62;
  TCCR1B |= (1 << WGM12) | (1 << CS12);
  TIMSK1 |= (1 << OCIE1A);  

  EICRB = (1<<ISC40) | (1<<ISC50) | (1<<ISC41) | (1<<ISC51);
  EIMSK = (1<<INT4) | (1<<INT5);
  sei();
}

ISR(INT4_vect) {
  if (timerRun == 1) {
    return;
  }
  timerTime=0;
  timerRun=1;
}


ISR(INT5_vect) {
  if (timerRun == 1) {
    return;
  }
  timerTime=0;
  timerRun=1;
}

ISR(TIMER1_COMPA_vect) {
  if (timerRun == 1) {
    int4State = TESTINT4;
    int5State = TESTINT5;
    timerTime++;
    if (timerTime >= 40) {
      if (int4State == 0b00010000) {
        int4active = true;
      } else if (int5State == 0b00100000) {
        int5active = true;
      }
      if ((int4State != 0b00010000) && (int5State != 0b00100000)) {
        if(timerTime >= 1000) {
          if (int4active) { 
            cnt += 10;
          } else if (int5active) {
            cnt -= 10;
          }
        } else {
          if (int4active) { 
            cnt ++;
          } else if (int5active) {
            cnt --;
          }
        }
        int4active = false;
        int5active = false;
        timerTime = 0;
        timerRun = 0;
      }
    }
  }
  if (cnt <= -10) {
    PORTE |= 0b00001000;
  } else if (cnt >= 10) {
    PORTG |= 0b00100000;
  } else {
    PORTE &= ~0b00001000;
    PORTG &= ~0b00100000;
  }
}

void loop() {
  lcd.clear();  
  lcd.setCursor(0, 0); // Set the cursor on the third column and first row.
  lcd.print("Count: ");
  lcd.print(cnt);
  Serial.print("Count: ");
  Serial.println(cnt);
  delay(500);
}
