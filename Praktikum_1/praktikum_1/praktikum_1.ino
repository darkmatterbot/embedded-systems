#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#define inc_btn 4         // D2 -> PORT-E 4
#define dec_btn 5         // D3 -> PORT-E 5
#define inputDT 1         // D52 -> PORT-C 1
#define inputCLK 0        // D53 --> PORT-C 0

LiquidCrystal_I2C lcd(0x27, 16, 2);

const uint8_t debounce = 10;        // Entprellzeit in ms
const uint8_t red_led = 5;          // D8 -> PORTH 5
const uint8_t blue_led = 6;         // D9 -> PORTH 6
const uint8_t ROWS[4] = {0,1,2,3};  // Die ersten 4 Bits vom PORT-C
const uint8_t COLS[4] = {7,6,5,4};  // Die letzten 4 Bits vom PORT-A

bool inc_pressed = false;
bool dec_pressed = false;
uint8_t pressTime = 0;
uint8_t row_index =0;
uint8_t col_index =0;
uint8_t current_clk;              // Drehzahlgeber
uint8_t previous_clk;             // Drehzahlgeber
String pressed_key = "";

volatile int cnt = 0;
volatile uint8_t red_frequency = 0;
volatile uint8_t blue_frequency = 0;
volatile int rotaryCounter =0;

void init_counter(){
  DDRE |= B00110000;      // Setze die bit 4 und 5 vom PORT-E als INPUT(0)
  PORTE &= B11001111;     // deaktivier pullup-resistor

  EICRB = B00001111;      // STEIGENDE Signale für INT4(D2) und INT5(D3)
  EIMSK = B00110000;      // Aktiviert Interrupts für int4(D2) and int5(D3)
}

void init_led_params(){
  DDRH |= B01100000;      // LEDS: bit 5 und 6 vom PORT-H als OUTPUT(1) setzen

  // Timer3 für Blaue LED, TIMSK3 und OCR3A werden im loop dynamisch gesetzt
  TCCR3A = 0;             // Setzt TCCR3A auf 0
  TCCR3B = B00000100;     // Prescaler 256

  // Timer4 für Rote LED, TIMSK4 und OCR4B werden im loop dynamisch gesetzt
  TCCR4A = 0;             // Setzt TCCR4A auf 0
  TCCR4B = B00000100;     // Prescaler 256

}
void init_matrix(){
  DDRA &= B00001111;      // Spalten: Setze die letzten 4 Bits vom PORT-A als INPUT(0)
  DDRC |= B00001111;      // Zeilen:  Setze die ersten 4 Bits vom PORT-C als OUTPUT(1)
  PORTC = (1 << ROWS[0]); // Direkt die erste Zeile auf HIGH(1) setzen
}

void init_rotary(){
  DDRB &= B00000011;      // Setze CLK und DT als INPUT(0)
  previous_clk = (PINE & (1 << inputCLK)) >> inputCLK; // lesen des initialwertes
}

void setup() {
  Serial.begin(9600);
  lcd.begin();
  cli();                  // interrupts deaktivieren

  init_counter();         // Die Knöpfe zum hoch/hunter-zählen
  init_led_params();      // LEDs mit per Knopf einstellbarer Blinkfrequenz Parametern
  init_matrix();          // Das 4*4 Tastenfeld
  init_rotary();          // Der Drehzahlgeber

  TCNT1 = 0;              // setze timer1(16bit) auf 0
  TCCR1B |= B00000001;    // kein(1)-prescaler notwendig
  TIMSK1 |= B00000010;    // aktiviert OCIE1A, also den compare match mode für OCR1A
  OCR1A = 16000;          // jede ms: 1(ms)*16.000.000 / ( 1 * 10^(3) ) = 16.000

  sei();                  // interrupts aktivieren
}

void loop() {
  lcd.clear();
  lcd.print("C"+String(cnt));               // Counter
  lcd.print(" S" +pressed_key);             // Tastenfeld
  lcd.print(" R"+String(rotaryCounter));    // Drehzahlgeber
  lcd.setCursor(0,1);                       // Nächscte Zeile
  lcd.print("Blue:" +String(blue_frequency));     // Blinkgeschwindigkeit Blau
  lcd.print(" Red:" +String(red_frequency));      // Blinkgeschwindigkeit Rot
  delay(500);
}

// Timer für Increment Button
ISR(INT4_vect) {
    inc_pressed= true;
}

// Timer für Decrement Button
ISR(INT5_vect) {
    dec_pressed = true;
}

// Timer für BLUE LED
ISR(TIMER3_COMPA_vect){
  TCNT3  = 0;                     // setze timer3 zurück
  PORTH ^= (1 << blue_led);       // Invertiere blaue led
}

// Timer für RED LED
ISR(TIMER4_COMPB_vect){
  TCNT4  = 0;                     // setze timer4 zurück
  PORTH ^= (1 << red_led);        // Invertiere rote led
}

// Dynamischen ändern der Blinkfrequenz der LEDs
void dynamic_compare_match(){
  TIMSK4 |= (1 << OCIE4B);        // aktivier compare match mode
  if(red_frequency == 0){
    TCNT4 = 0;
    TIMSK4 &= ~(1 << OCIE4B);     // deaktivier compare match mode -> keine interrupts
    PORTH &= ~(1 << red_led);     // Aktivier rote led,
  }
  else if(red_frequency ==1){
    OCR4B = 62500;                // 1(s)*16.000.000/256 -> blinkt jede Sekunde
  }
  else if(red_frequency==2){
    OCR4B = 31250;                // 500(ms)*16.000.000/256*10^3 -> blinkt alle 500ms
  }
  else if(red_frequency==3){
    OCR4B = 6250;                 // 100(ms)*16.000.000/256*10^3 -> blinkt alle 100ms
  }

  TIMSK3 |= (1 << OCIE3A);        // aktivier compare match mode
  if(blue_frequency == 0){
    TCNT3 = 0;
    TIMSK3 = 0;                   // deaktivier compare match mode -> keine interrupts
    PORTH &= ~(1 << blue_led);    // Aktivier blue led
  }
  else if(blue_frequency == 1){
    OCR3A = 62500;                // 1(s)*16.000.000/256 -> blinkt jede Sekunde
  }
  else if(blue_frequency == 2){
    OCR3A = 31250;                // 500(ms)*16.000.000/256*10^3 -> blinkt alle 500ms
  }
  else if(blue_frequency == 3){
    OCR3A = 6250;                 // 100(ms)*16.000.000/256*10^3 -> blinkt alle 100ms
  }
}
// Timer Interrupt, wird jede ms aufgerufen
// Wird von den Counter-Buttons, LED-Params, vom Tastenfeld und vom Drehzahlgeber verwendet
ISR(TIMER1_COMPA_vect){

  /* START COUNTER REGION */
    /* Increment section */
    if(inc_pressed){
      pressTime++;
      /* Long press? */
      if( (PINE & _BV(inc_btn)) >> inc_btn ){ // 1: gedrückt, 0:nada
        if(pressTime > 1000){
          cnt += 10;
          pressTime = 0;
          red_frequency = 0;
        }
      }
      /* Short press? */
      else if(pressTime > debounce){
        cnt++;
        pressTime = 0;
        inc_pressed = false;
        red_frequency = (red_frequency+1) %4;
      }
    }

    /* Decrement section */
    else if(dec_pressed){
      pressTime++;
      /* Long press? */
      if( (PINE & _BV(dec_btn)) >> dec_btn ){ // 1: gedrückt, 0:nada
        if(pressTime > 1000){
          cnt -= 10;
          pressTime = 0;
          blue_frequency = 0;
        }
      }
      /* Short press? */
      else if(pressTime > debounce){
        cnt--;
        pressTime = 0;
        dec_pressed = false;
        blue_frequency = (blue_frequency+1) %4;
      }
    }
  dynamic_compare_match();
  /* ENDE COUNTER REGION */

  /* 4*4 Tastenfeld Region */
  // Welche der 4 Spalten wurde gedrückt?
  if( PINA & (1 << COLS[col_index]) ){
    pressed_key = row_index*4 + col_index + 1;  // Tastenfeld[row_index][col_i];
  }

  col_index = (col_index+1) %4;                 // Spaltenindex für nächsten Interrupt auswählen
  if(col_index == 0){
    row_index = (row_index+1) %4;               // Näcshte Reihe auf HIGH(1) setzen
    PORTC = (1 << ROWS[row_index]);
  }
  /* ENDE 4*4 Tastenfeld Region */

  /* START Drehzahlgeber Region */
  current_clk = (PINB & (1 << inputCLK)) >> inputCLK;

  // Wenn der letzte und der aktuelle Pin-Status uugleich, dann wurde gedreht
  if (current_clk != previous_clk && current_clk == HIGH){

      // Wenn inputDT ungleich inputCLK, dann wurde gegen den Uhrzeigersinn gedreht
      if ( ((PINB & (1 << inputDT)) >> inputDT) != current_clk) {
        rotaryCounter --;
      }
      else {
        // Ansonsten im Uhrzeigersinn
        rotaryCounter ++;
      }
   }
   previous_clk = current_clk;                // Merke den letzten Pin-Status
   /* ENDE Drehzahlgeber Region */

  TCNT1 = 0;                                  // Setze timer1 wieder auf 0
}
